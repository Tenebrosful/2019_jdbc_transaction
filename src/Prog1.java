import java.sql.*;

import java.io.*;

public class Prog1 {

	public static void main(String[] args) throws Exception {

		String url = "jdbc:oracle:thin:@charlemagne.iutnc.univ-lorraine.fr:1521:infodb";
		Class.forName("oracle.jdbc.driver.OracleDriver");

		Connection connexion1 = DriverManager.getConnection(url, "allard31u", "martinetclem");
		Connection connexion2 = DriverManager.getConnection(url, "allard31u", "martinetclem");

		// Cr�ation de la table TRANSACTION
		connexion1.setAutoCommit(false);
		Statement st1 = connexion1.createStatement();
		st1.executeUpdate("CREATE TABLE TRANSACTION (tid number(2,0) not null, lib varchar2(20), primary key(tid) )");

		// lancer select * from TRANSACTION;
		Statement st2 = connexion2.createStatement();
		ResultSet rs2 = st2.executeQuery("Select * from TRANSACTION");

	}
}